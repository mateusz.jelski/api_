
import requests

#f = requests.get("https://api.disneyapi.dev/characters/419")
# def parse_cmd(cmd,arg_spacer,fetch):
#     target_l = []
#     args_l = []
#     all_l = []
#
#     c_string = ""
#
#     iter_ = 0
#     while (iter_ < len(cmd)):
#         add = True
#
#         if (cmd[iter_] == arg_spacer):
#             if (fetch == 0):
#                 args_l.append(cmd[iter_:iter_+2])
#             elif (fetch == 1):
#                 all_l.append(cmd[iter_:iter_+2])
#             iter_ += 1
#             c_string = ""
#             add = False
#         elif (cmd[iter_] == " "):
#             if(c_string != ""):
#                 if (fetch == 0):
#                     target_l.append(c_string)
#                 elif (fetch == 1):
#                     all_l.append(c_string)
#                 c_string = ""
#                 add = False
#             else:
#                 add = False
#         if (add == True):
#             c_string += cmd[iter_]
#         iter_ += 1
#     if (c_string != ""):
#         if (fetch == 0):
#             target_l.append(c_string)
#         elif (fetch == 1):
#             all_l.append(c_string)
#     if (fetch == 0):
#         return (target_l,args_l)
#     elif (fetch == 1):
#         return all_l

CONST_ERR_CODES = ("cannot obtain page and character in one request","Incorrect request","Needs a word to highlight")
DATA_STRUCTURE = ('films', 'shortFilms', 'tvShows', 'videoGames', 'parkAttractions', 'allies', 'enemies', '_id', 'sourceUrl', 'name', 'imageUrl', 'createdAt', 'updatedAt', 'url', '__v')
def err_code_calculator(code):
    return CONST_ERR_CODES[code]

def hightlight_string(string,word,color):
    reset_color = '\033[0m'
    new_string = ""
    iter_ = 0
    while iter_ != len(string) - len(word) + 1:
        if (string[iter_:iter_+len(word)] == word):
            new_string+= color
            new_string+= word
            new_string+= reset_color
            iter_ += len(word)-1
        else:
            new_string += string[iter_]
        iter_ += 1
    return new_string

#print(hightlight_string("hghghgcvhghgdfcvv","cv","\u001b[44;1m")) # \u001b[44;1m - blue

def rGlue(list):
    if (list == []):
        return ""
    return rGlue(list[1:]) + str(list[0]) + ", "

def to_string(dict_,data_str):
    mstring = ""
    for l in dict_:

        ostr = ""
        for i in data_str:
            ostr += i
            ostr += ":"
            if (type(l[i]) == list):
                ostr += rGlue(l[i])
            else:
                ostr += str(l[i])
            ostr += "\n"
        mstring += "\n"
        mstring += ostr
    return mstring

def print_color(color,text):
    print(color+text,"\u001b[0m") # "\u001b[42m" - green

class core:

    def __init__(self):
        self.page_req = "https://api.disneyapi.dev/characters?page="
        self.char_req = "https://api.disneyapi.dev/characters/"
        self.last_req = ""
        self.last_err = -1

    def req_construct(self,type,args):
        out_req = ""

        if (type == "character"):
            out_req += self.char_req
            out_req += args[-1]
        elif (type == "page"):
            out_req += self.page_req
            out_req += args[-1]
        return out_req

    def soft_cmd(self,args):
        if (args[0] == "buffer"):
            for i in range(len(args[1:])):
                if (args[i] == "/s"):
                    self.show()
                elif (args[i] == "/c"):
                    self.last_req = ""
                elif (args[i] == "/h"):
                    try:
                        print(hightlight_string(self.last_req,args[i+1],'\u001b[44;1m'))
                    except:
                        self.last_err = 2
            return -1
        return 0

    def req_get(self,args):
        if (self.soft_cmd(args) == -1):
            return 0
        chr_per_page = 50
        data_str_ = DATA_STRUCTURE[:-5]

        nxt_pass = False
        for i in range(len(args))[1:]:
            if (nxt_pass == False):

                if (args[i] == "/s"): #s - ilość wyświetlaanych filmów
                    chr_per_page = int(args[i+1])
                    nxt_pass = True
                elif (args[i] == "/d"):
                    data_str_ = DATA_STRUCTURE[:int(args[i+1])]
            else:
                nxt_pass = False

        try:
            f = self.req_construct(args[0],args[1:])
            print(f)
            self.last_req = requests.get(f)
            self.last_req = to_string(self.last_req.json()["data"][:chr_per_page],data_str_)
            #self.last_req = character
            self.last_err = -1
        except:
            self.last_err = 1
        self.check_err()

    def check_err(self):
        if (self.last_err == -1):
            self.show()
        else:
            self.last_err = err_code_calculator(self.last_err)
            self.show_err()
        
    def show(self):
        print_color("",self.last_req)

    def show_err(self):
        print_color("\u001b[41m", self.last_err)
        self.last_err = -1

    def clear(self):
        self.last_req = ""

def parse_cmd_all(cmd,arg_spacer):
    all_l = []

    c_string = ""

    iter_ = 0
    while (iter_ < len(cmd)):
        add = True

        if (cmd[iter_] == arg_spacer):
            all_l.append(cmd[iter_:iter_+2])
            iter_ += 1
            c_string = ""
            add = False
        elif (cmd[iter_] == " "):
            if(c_string != ""):
                all_l.append(c_string)
                c_string = ""
                add = False
            else:
                add = False
        if (add == True):
            c_string += cmd[iter_]
        iter_ += 1
    if (c_string != ""):
        all_l.append(c_string)
    return all_l

def mainloop():
    CORE = core()

    while True:

        inp = parse_cmd_all(str(input(": ")),"/")
        CORE.req_get(inp)



        #CORE.clear()

mainloop()
